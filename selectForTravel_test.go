package afonasiy_hero

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

//Тестирует на выбор ниаиподходящего способа уничтожения
func TestStock_SelectWeaponFromStok_SelectTheBeastWeapon(t *testing.T) {

	weapons := map[string][]*Weapon{}

	stData := struct {
		Weapons map[string][]*Weapon
	}{weapons}
	stock := Stock(stData)

	weaponsForQ := GetWeaponsForQuest()
	for _, el := range weaponsForQ {
		stock.AddToStock(el)
	}

	monstersForQ := GetMonstersForQuest()
	weapon := *SelectBestWeapon(monstersForQ[0], &stock)
	//bag := SelectForTravel(monstersForQ, &stock)
	//_, res := bag.Get("Тапок")

	//assert.True(t, res)
	assert.Equal(t,"Тапок", weapon.GetName())
}

func TestStock_SelectWeaponForRavel_SelectTheBeastWeapon(t *testing.T) {
	weapons := map[string][]*Weapon{}

	stData := struct {
		Weapons map[string][]*Weapon
	}{weapons}
	stock := Stock(stData)

	weaponsForQ := GetWeaponsForQuest()
	for _, el := range weaponsForQ {
		stock.AddToStock(el)
	}

	monstersForQ := GetMonstersForQuest()
	bag := SelectForTravel(monstersForQ, &stock)
	weapon, res := bag.Get("Тапок")

	assert.True(t, res)
	assert.Equal(t,"Тапок", weapon.GetName())
}
