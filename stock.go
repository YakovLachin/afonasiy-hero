package afonasiy_hero

type Stock struct {
	Weapons map[string][]*Weapon
}

func (s *Stock) AddToStock(weapon *Weapon) {
	key := weapon.TypeOfWeapon
	s.Weapons[key] = append(s.Weapons[key], weapon)
}

func (s *Stock) getWeaponsListByKey(key string) []*Weapon {
	return s.Weapons[key]
}

func (s *Stock) Init() *Stock {
	s.Weapons = map[string][]*Weapon{}
	return s
}