package afonasiy_hero

const (
	MelleeWeapon     string = "Ближни бой"
	MeedlerangeWeapon string = "Бой средней дистанции"
	LongRangeWeapon   string = "Бой с двльней дистанции"
)

type Weapon struct {
	name         string
	desc         string
	damage       int
	TypeOfWeapon string
}

func (w *Weapon) GetName() string {
	return w.name
}

func (w *Weapon) SetName(name string) {
	w.name = name
}

func (w *Weapon) GetDamage() int {
	return w.damage
}