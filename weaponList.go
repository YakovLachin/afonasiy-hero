package afonasiy_hero

func GetWeaponsForQuest() []*Weapon{
	weapons := make ([]*Weapon, 3)
	weapons[0] = &Weapon {
		"Охотничий Нож",
		"Весомый аргумент в комунальных переговорах",
		30,
		MelleeWeapon,
	}

	weapons[1] = &Weapon{
		"Тапок",
		"Старый дедовский способ",
		5,
		MelleeWeapon,
	}

	weapons[2] = &Weapon {
		"Нагината",
		"Этим 2-х метровым шестом с 30-ти сантиметровй косой на конце японцы нагибают недругов с конца VII века",
		35,
		MeedlerangeWeapon,
	}

	return weapons
}