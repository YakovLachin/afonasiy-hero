package afonasiy_hero

func SelectForTravel(monsters []*Monster, stock *Stock) *Bag {
	items := map[string] *Weapon {}
	bagData := struct {
		Items map[string] *Weapon
	}{
		items,
	}
	bag := Bag(bagData)
	for _, monster := range monsters {
		weapon := SelectBestWeapon(monster, stock)
		weaponVal := *weapon
		bag.Add(weaponVal.name, weapon)
	}

	return &bag
}

// Выбирает наиболее подходящее оружие против монстра
func SelectBestWeapon (pointMonster *Monster, stock *Stock) *Weapon {
	//в Переменную записываются максимально подходящий урон для моба
	// должен стремиться у нулю, но не быть меньше нуля.
	// стирается в первой итерации
	monster := *pointMonster
	nominalDifference := -1
	needWeaponPointer := &Weapon{}
	weapons := stock.getWeaponsListByKey(monster.typeOfMonster)
	for _, pointer := range weapons {
		weapon := *pointer
		if weapon.damage < monster.Healths  {
			continue
		}
		difference := weapon.damage - monster.Healths

		if nominalDifference == -1 || (difference < nominalDifference && difference > 0) {
			needWeaponPointer = pointer
			nominalDifference = difference
		}
	}

	return needWeaponPointer
}