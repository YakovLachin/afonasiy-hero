package afonasiy_hero

// Та самая прекрасная сумка, которая будет внутри себя
// хранить все средства истребления бедных монстров.
//
type Bag struct {
	Items map[string] *Weapon
}

// Как и в любое другое добавляемое, в сумочку можно че-то положить
// Например .... МБР?
func (h *Bag) Add(key string, value *Weapon) *Bag {
	h.Items[key] = value

	return h
}

// Зная волшебное слово можно достать
// то что было положено и забыто ранее.
func (h *Bag) Get(key string) (*Weapon, bool){
	weapon, err := h.Items[key]
	if err {
		delete(h.Items, key)
		return weapon, err
	}
	return nil, err
}

//Отображает общий список того, что у нас в сумке
func (b *Bag) ListWeapons()  map[string] *Weapon {
	return b.Items
}