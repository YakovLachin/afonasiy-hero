package afonasiy_hero

//10 самых безобидных существ, которых пытаются убить бедные путники.
func GetMonstersForQuest() []*Monster {
	monsters := make([]*Monster, 1)

	monsters[0] = &Monster{
		"Розовый кролик",
		"3-4 кг ярости в клубке розового меха",
		Mellee,
		1,
	}

	return monsters
}