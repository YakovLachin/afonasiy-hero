package afonasiy_hero

func SelectWeapon(monster *Monster, hero *Hero) {
	bag := hero.GetBag()
	list := bag.ListWeapons()

	nameOfBestWeapon := ""

	nominalDifference := -1
	for _, weapon := range list {
		if weapon.TypeOfWeapon != monster.typeOfMonster {
			continue
		}

		difference := weapon.damage - monster.Healths

		if nominalDifference == -1 || (difference < nominalDifference && difference > 0) {
			nameOfBestWeapon = weapon.GetName()
			nominalDifference = difference
		}
	}

	hero.PutInArmsFromBag(nameOfBestWeapon)
}