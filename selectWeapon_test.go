package afonasiy_hero

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

//Тестирует, что для кролика досточно и одного тапка.
func TestSelectWeapon(t *testing.T) {
	weapons := make(map[string] *Weapon)
	bagData := struct {
		Items map[string] *Weapon
	}{weapons}

	bag := Bag(bagData)

	weapon := Weapon{ "Тапок", "Сатрый Дедовский способ", 5, MelleeWeapon }
	bag.Add("Тапок", &weapon)
	rocket := Weapon{ "МБР", "Межконтинентальная балистическая ракета", 1000, LongRangeWeapon }
	bag.Add("МБР", &rocket)
	arms := Weapon {
		"руки",
		"60% тренировочного времни разведчика составляет рукопашный бой. После 15 лет выслуги, дадим поблажу.",
		1,
		LongRangeWeapon,
	}

	afonyaData := struct {
		Arms *Weapon
		bag Bag
	}{
		&arms,
		bag,
	}
	afonya := Hero(afonyaData)

	mosters := GetMonstersForQuest()
	SelectWeapon(mosters[0], &afonya)
	assert.Equal(t, weapon, *afonya.Arms, "У Афони до сих пор голые руки")
}