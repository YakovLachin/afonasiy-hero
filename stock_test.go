package afonasiy_hero

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

//Тест на то что на склад можно полоожить что нить
func TestStock_AddToStock(t *testing.T) {
	weapon := Weapon{
		"Morgenstern",
		"Шипастый шар на кованой цепи и палице, получивший имя Утреняя звезда. С добрым утром как говориться)",
		300,
		MeedlerangeWeapon,
	}
	weapon1 := Weapon{
		"Morgenstern",
		"Шипастый шар на кованой цепи и палице, получивший имя Утреняя звезда. С добрым утром как говориться)",
		300,
		MeedlerangeWeapon,
	}
	weapon2 := Weapon{
		"Morgenstern",
		"Шипастый шар на кованой цепи и палице, получивший имя Утреняя звезда. С добрым утром как говориться)",
		300,
		LongRangeWeapon,
	}

	weapons := map[string][]*Weapon{}
	stData := struct {
		Weapons map[string][]*Weapon
	}{weapons}

	stock := Stock(stData)
	stock.AddToStock(&weapon)
	stock.AddToStock(&weapon1)
	stock.AddToStock(&weapon2)
	assert.Equal(t, weapon, *stock.Weapons[MeedlerangeWeapon][0])
	assert.Equal(t, weapon1, *stock.Weapons[MeedlerangeWeapon][1])
	assert.Equal(t, weapon2, *stock.Weapons[LongRangeWeapon][0])
}