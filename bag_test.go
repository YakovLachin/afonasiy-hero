package afonasiy_hero

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

//Тест на то, что Наш Афоня, достанет из сумки то же самое, что положил в нее несколько ранее
// без видимых приколов со стороны автора.
func TestBag_AddAndGetWeapon_WeaponAddedAndGettedSucсsied(t *testing.T) {
	weapons := make(map[string] *Weapon)
	bagData := struct {
		Items map[string] *Weapon
	}{weapons}

	bag := Bag(bagData)
	weapon := Weapon{ "Кусанаги-но цуруги", "Символ власти яонских императоров. Был найден в теле убитого восьмиглавого дракона", 505, LongRangeWeapon }
	bag.Add("Карман", &weapon)
	actual, err := bag.Get("Карман")
	assert.Equal(t, weapon, *actual, "Не эквивалентны")
	assert.True(t, err)
}

//Тест на то, что Афоня не может быть читером, и не достанет из одного карманма одну и туже вещь дважды.
func TestBag_AddAndGetWeapon_BagAfterGettingIsEmpty(t *testing.T) {
	weapons := make(map[string] *Weapon)
	bagData := struct {
		Items map[string] *Weapon
	}{weapons}

	bag := Bag(bagData)
	weapon := Weapon{ "LongSword Flamberg", "Сила и красота в одном изделии. Предмет пафоса любого рыцаря. ", 500, MeedlerangeWeapon }
	bag.Add("LongSword Flamberg", &weapon)
	_, res := bag.Get("LongSword Flamberg")
	assert.True(t, res)
	_, res = bag.Get("LongSword Flamberg")
	assert.False(t, res)
}
