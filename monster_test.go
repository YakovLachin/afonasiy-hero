package afonasiy_hero

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

//Тест на то что монстру можно дать пинка подзад и он это почувствует
func TestMonster_DecreaseHealths_HealthsAreDecreased(t *testing.T) {
	mData := struct {
		name          string
		description   string
		typeOfMonster string
		Healths       int
	}{
		"Черный дракон",
		"Большой, клыкастый, жутко злой паразит, пугающий все фэнтезийные вселенные",
		LongRange,
		300,
	}

	monster := Monster(mData)
	monster.DecreaseHealths(100)
	assert.Equal(t, 200, monster.Healths)
}