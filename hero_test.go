package afonasiy_hero


import (
	"testing"
	"github.com/stretchr/testify/assert"
)

//Тест На то что Афоня не дрызщ и может взять в руки меч когда это понадобится
func TestHero_PutInArmsFromBag_WeaponInArms(t *testing.T) {
	weapon := Weapon {
		"Фальшион",
		"Типовое оружие фронтового пехотинца 14-го века. По силе рубящего удара, не многим отличается от топора",
		300,
		LongRangeWeapon,
	}

	weapons := make(map[string] *Weapon)
	bagData := struct {
		Items map[string] *Weapon
	}{weapons}
	bag := Bag(bagData)
	bag.Add("Фальшион", &weapon)

	arms := Weapon {
		"руки",
		"60% тренировочного времни разведчика составляет рукопашный бой. После 15 лет выслуги, дадим поблажу.",
		1,
		LongRangeWeapon,
	}

	afonyaData := struct {
		Arms *Weapon
		bag Bag
	}{
		&arms,
		bag,
	}
	afonya := Hero(afonyaData)

	afonya.PutInArmsFromBag("Фальшион")

	assert.Equal(t, weapon, *afonya.Arms, "У Афони до сих пор голые руки")
	assert.NotEqual(t, arms, *afonya.Arms, "У Афони до сих пор голые руки")
}

//Тест На то что Афоня - не чукча, и не выбросит собственные руки когда в мешке ничего нет
func TestHero_PutNotExistWeaponInArmsFromBag_NotE(t *testing.T) {
	weapon := Weapon {
		"Семизубый меч",
		"Национальное сокровище японии. Видать в семи клинках есть какой-то смысл..",
		150 * 7,
		LongRangeWeapon,
	}

	weapons := make(map[string] *Weapon)
	bagData := struct {
		Items map[string] *Weapon
	}{weapons}
	bag := Bag(bagData)
	bag.Add("Семизубый меч", &weapon)

	arms := Weapon {
		"руки",
		"60% тренировочного времни разведчика составляет рукопашный бой. После 15 лет выслуги, дадим поблажу.",
		1,
		LongRangeWeapon,
	}

	afonyaData := struct {
		Arms *Weapon
		bag Bag
	}{
		&arms,
		bag,
	}
	afonya := Hero(afonyaData)

	afonya.PutInArmsFromBag("Семизубый меч")

	assert.Equal(t, weapon, *afonya.Arms, "У Афони до сих пор голые руки")
	assert.NotEqual(t, arms, *afonya.Arms, "У Афони до сих пор голые руки")
}