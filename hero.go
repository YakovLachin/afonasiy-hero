package afonasiy_hero

// Афанасий - герой, идущий тропой судьбы
// в сторону города Мастера Меча
type Hero struct {
	Arms *Weapon
	bag Bag
}

// Берёт в руки, то что можно достать из сумки.
func (h *Hero) PutInArmsFromBag(slot string) {
	weapon, res := h.bag.Get(slot)
	if res {
		h.Arms = weapon
	}
}

//Показывает Свою сумку
func (h *Hero) GetBag() *Bag{
	return &h.bag
}
//Показывает Свою сумку
func (h *Hero) SetBag(b Bag) {
	h.bag = b
}