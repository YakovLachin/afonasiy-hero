package afonasiy_hero

const (
	Mellee      string = "Ближни бой"
	Meedlerange string = "Бой средней дистанции"
	LongRange   string = "Бой с двльней дистанции"
)

//Структура существа подлежащего уничтожению
type Monster struct {
	name          string
	description   string
	typeOfMonster string
	Healths       int
}

func (m *Monster) DecreaseHealths(value int) {
	m.Healths = m.Healths - value
}